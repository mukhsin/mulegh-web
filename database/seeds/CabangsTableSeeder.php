<?php

use Illuminate\Database\Seeder;

class CabangsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cabangs')->delete();
        
        \DB::table('cabangs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kode' => 'MP',
                'nama' => 'PURWOKERTO',
                'alamat' => 'PURWOKERTO',
                'telepon' => '12345',
                'created_at' => '2016-10-26 15:18:19',
                'updated_at' => '2016-10-27 09:12:30',
            ),
        ));
        
        
    }
}
