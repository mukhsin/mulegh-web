<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'username' => 'BENI',
                'password' => '$2y$10$RYVZuxDeKEsQUHtke4TR1.Bb4aBuoU1YN4ohZc9P37eNEtNBpbn2W',
                'nama' => 'OM BENI',
                'role' => 'bos',
                'cabang_id' => 1,
                'api_token' => 'T5F8N0KHC3wRkyRTj0c9CQFK6UBVb0KQRzqtiZOEMtqECmcgnHOXpvHByzFA',
                'remember_token' => NULL,
                'created_at' => '2016-10-27 09:02:12',
                'updated_at' => '2016-10-27 09:02:12',
            ),
            1 => 
            array (
                'id' => 2,
                'username' => 'MUKHSIN',
                'password' => '$2y$10$H3p.cQq/AdfqeXqVLerRiuTuLZqnJNABZM/qgq362qrcQXpOKOPtu',
                'nama' => 'MUKHAMAD MUKHSIN WIBOWO',
                'role' => 'admin',
                'cabang_id' => 1,
                'api_token' => 'aOp7QFGteQez7gZd8LZhHPMwMYstnnzBwDkJAHXCZl7LK910EQA86eCEe7Ze',
                'remember_token' => '5X7hHasjcsSc3HAMBi6bfIATvhWQNkunbFsJZJjcpBfwUO8vG1QXuJ9M8zhp',
                'created_at' => '2016-10-27 09:02:13',
                'updated_at' => '2016-10-27 09:12:09',
            ),
        ));
        
        
    }
}
