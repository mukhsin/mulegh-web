<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleWithAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (in_array($request->user()->role, explode('|', $role))) {
            return $next($request);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Anda tidak berhak mengakses laman ini.',
            ]);
        }
    }
}
