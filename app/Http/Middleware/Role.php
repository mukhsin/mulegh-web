<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (in_array(Auth::user()->role, explode('|', $role))) {
            return $next($request);
        } else {
            return redirect('/');
        }

        // if (Auth::user()->role != $role) {
        //     return redirect('/');
        // }
        //
        // return $next($request);
    }
}
