<?php

namespace App\Http\Controllers;

use App\Botol;
use App\Http\Requests;
use App\Stock;
use App\Utils;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BotolStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $botol = Botol::find($id);
        $stocks = array();
        foreach ($botol->stocks()->orderBy('updated_at', 'desc')->get() as $stock) {
            array_push($stocks, $stock);
        }

        return view('botol.stock.index', [
            'botol' => $botol,
            'stocks' => Utils::paginate($stocks, 10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('botol.stock.create', ['botol' => Botol::find($id)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'tipe' => 'required|max:255',
            'awal' => 'required|max:255',
            'order' => 'required|max:255',
            'akhir' => 'required|max:255',
        ]);

        $order = $request->plus == 1 ? $request->order : -$request->order;

        $stock = Stock::create([
            'botol_id' => $id,
            'awal' => $request->awal,
            'order' => $order,
            'akhir' => $request->akhir,
            'keterangan' => $request->keterangan,
            'status' => 1,
            'author' => Auth::user()->username,
        ]);

        $botol = Botol::find($id);
        $botol->stock = $request->akhir;
        $botol->save();

        if ($stock && $botol) {
            return redirect(url('/botol/'.$id.'/stock'))->with('sukses', 'Berhasil menambah stock.');
        } else {
            return redirect(url('/botol/'.$id.'/stock'))->with('gagal', 'Terjadi kesalahan saat menambah stock.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $idstock)
    {
        // return view('botol.stock.detail', [
        //     'stock' => Stock::find($idstock),
        //     'botol' => Botol::find($id),
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
