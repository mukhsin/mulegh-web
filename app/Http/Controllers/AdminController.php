<?php

namespace App\Http\Controllers;

use App\Cabang;
use App\Http\Requests;
use App\User;
use App\Utils;
use Auth;
use Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = array();
        foreach (User::where('role', 'admin')->get() as $admin) {
            array_push($admins, $admin);
        }

        return view('admin.index', [
            'admins' => Utils::paginate($admins, 10),
            'query' => '',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create', ['cabangs' => Cabang::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|max:255|unique:users',
            'nama' => 'required|max:255',
            'cabang_id' => 'required|max:255',
        ]);

        $user = User::create([
            'username' => $request->username,
            'password' => Hash::make($request->username.'12345'),
            'nama' => $request->nama,
            'role' => 'admin',
            'cabang_id' => $request->cabang_id,
            'api_token' => str_random(60),
        ]);

        if ($user) {
            return redirect(url('/admin'))->with('sukses', 'Berhasil menambah admin.');
        } else {
            return redirect(url('/admin'))->with('gagal', 'Terjadi kesalahan saat menambah admin.');
        }
    }

    /**
     * Display the searched resource.
     *
     * @param  int  $query
     * @return \Illuminate\Http\Response
     */
    public function search($query)
    {
        $array = User::where('username', 'like', $query)
                        ->orWhere('username', 'like', $query.'%')
                        ->orWhere('username', 'like', '%'.$query)
                        ->orWhere('username', 'like', '%'.$query.'%')
                        ->orWhere('nama', 'like', $query)
                        ->orWhere('nama', 'like', $query.'%')
                        ->orWhere('nama', 'like', '%'.$query)
                        ->orWhere('nama', 'like', '%'.$query.'%')
                        ->get();

        $admins = array();
        foreach ($array as $a) {
            array_push($admins, $a);
        }

        return view('admin.index', [
            'admins' => Utils::paginate($admins, 10),
            'query' => $query,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.show', ['admin' => User::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit', [
            'admin' => User::find($id),
            'cabangs' => Cabang::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'cabang_id' => 'required|max:255',
        ]);

        $admin = User::find($id);
        $admin->cabang_id = $request->cabang_id;
        $admin->save();

        if ($admin) {
            return redirect(url('/admin'))->with('sukses', 'Berhasil mengubah admin.');
        } else {
            return redirect(url('/admin'))->with('gagal', 'Terjadi kesalahan saat mengubah admin.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = User::find($id);
        $admin->delete();

        if ($admin) {
            return redirect(url('/admin'))->with('sukses', 'Berhasil menghapus admin.');
        } else {
            return redirect(url('/admin'))->with('gagal', 'Terjadi kesalahan saat menghapus admin.');
        }
    }
}
