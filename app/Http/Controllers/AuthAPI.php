<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AuthAPI extends Controller
{
    public function login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        $user = User::where('username', $username)->first();

        if ($user) {
            if (Hash::check($password, $user->password)) {
                return response()->json([
                    'error' => false,
                    'message' => 'Berhasil masuk',
                    'user' => $user
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'Alamat E-Mail dengan password tidak cocok'
                ]);
            }
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Tidak tersedia pengguna dengan alamat E-Mail tersebut'
            ]);
        }
    }
}
