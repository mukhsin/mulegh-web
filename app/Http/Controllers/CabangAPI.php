<?php

namespace App\Http\Controllers;

use App\Cabang;
use App\Http\Requests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CabangAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'error' => false,
            'cabangs' => Cabang::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('cabang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cabang = Cabang::create([
            'kode' => $request->kode,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'telepon' => $request->telepon,
        ]);

        if ($cabang) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menambah cabang.',
                'cabangs' => Cabang::all(),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menambah cabang.',
                'cabangs' => Cabang::all(),
            ]);
        }
    }

    /**
     * Display the searched resource.
     *
     * @param  int  $query
     * @return \Illuminate\Http\Response
     */
    public function search($query)
    {
        $cabangs = Cabang::where('kode', 'like', $query)
                        ->orWhere('kode', 'like', $query.'%')
                        ->orWhere('kode', 'like', '%'.$query)
                        ->orWhere('kode', 'like', '%'.$query.'%')
                        ->orWhere('nama', 'like', $query)
                        ->orWhere('nama', 'like', $query.'%')
                        ->orWhere('nama', 'like', '%'.$query)
                        ->orWhere('nama', 'like', '%'.$query.'%')
                        ->orWhere('alamat', 'like', $query)
                        ->orWhere('alamat', 'like', $query.'%')
                        ->orWhere('alamat', 'like', '%'.$query)
                        ->orWhere('alamat', 'like', '%'.$query.'%')
                        ->orWhere('telepon', 'like', $query)
                        ->orWhere('telepon', 'like', $query.'%')
                        ->orWhere('telepon', 'like', '%'.$query)
                        ->orWhere('telepon', 'like', '%'.$query.'%')
                        ->get();

        return response()->json([
            'error' => false,
            'cabangs' => $cabangs,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'error' => false,
            'cabang' => Cabang::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return view('cabang.edit', ['cabang' => Cabang::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cabang = Cabang::find($id);
        $cabang->kode = $request->kode;
        $cabang->nama = $request->nama;
        $cabang->alamat = $request->alamat;
        $cabang->telepon = $request->telepon;
        $cabang->save();

        if ($cabang) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil mengubah cabang.',
                'cabangs' => Cabang::all(),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat mengubah cabang.',
                'cabangs' => Cabang::all(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cabang = Cabang::find($id);
        $cabang->delete();

        if ($cabang) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menghapus cabang.',
                'cabangs' => Cabang::all(),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menghapus cabang.',
                'cabangs' => Cabang::all(),
            ]);
        }
    }
}
