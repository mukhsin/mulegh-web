<?php

namespace App\Http\Controllers;

use App\Botol;
use App\Http\Requests;
use App\Stock;
use App\Utils;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BotolStockAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $botol = Botol::find($id);
        $stocks = array();
        foreach ($botol->stocks()->orderBy('updated_at', 'desc')->get() as $stock) {
            array_push($stocks, $stock);
        }

        return response()->json([
            'error' => false,
            'botol' => $botol,
            'stocks' => Utils::paginate($stocks, 10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return response()->json([
            'error' => false,
            'botol' => Botol::find($id),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $order = $request->plus == 1 ? $request->order : -$request->order;
        $stock = Stock::create([
            'botol_id' => $id,
            'awal' => $request->awal,
            'order' => $order,
            'akhir' => $request->akhir,
            'keterangan' => $request->keterangan,
            'status' => 1,
            'author' => $request->user()->username,
        ]);

        $botol = Botol::find($id);
        $botol->stock = $request->akhir;
        $botol->save();

        $stocks = array();
        foreach ($botol->stocks()->orderBy('updated_at', 'desc')->get() as $stock) {
            array_push($stocks, $stock);
        }

        if ($stock && $botol) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menambah stock.',
                'botol' => $botol,
                'stocks' => Utils::paginate($stocks, 10),
            ]);
        } else {
            return response()->json([
                'error' => false,
                'message' => 'Terjadi kesalahan saat menambah stock.',
                'botol' => $botol,
                'stocks' => Utils::paginate($stocks, 10),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $idstock)
    {
        // return view('botol.stock.detail', [
        //     'stock' => Stock::find($idstock),
        //     'botol' => Botol::find($id),
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
