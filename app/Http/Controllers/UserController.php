<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getProfil()
    {
        return view('user/profil', ['user' => Auth::user()]);
    }

    public function putProfil(Request $request)
    {
        $this->validate($request, ['nama' => 'required|max:255']);

        $user = Auth::user();
        $user->nama = $request->nama;
        $user->save();

        if ($user) {
            return redirect('/profil')->with('sukses', 'Berhasil mengubah profil.');
        } else {
            return redirect('/profil')->with('gagal', 'Terjadi kesalahan saat mengubah profil.');
        }
    }

    public function getSandi()
    {
        return view('user/sandi', ['user' => Auth::user()]);
    }

    public function putSandi(Request $request)
    {
        $this->validate($request, [
            'sandi_lama' => 'required|min:6',
            'sandi_baru' => 'required|min:6|confirmed',
        ]);

        $user = Auth::user();
        if (Hash::check($request->sandi_lama, $user->password)) {
            $user->password = Hash::make($request->sandi_baru);
            $user->save();

            if ($user) {
                return redirect('/sandi')->with('sukses', 'Berhasil mengubah password.');
            } else {
                return redirect('/sandi')->with('gagal', 'Terjadi kesalahan saat mengubah password.');
            }
        } else {
            return redirect('/sandi')->with('gagal', 'Password lama tidak benar.');
        }
    }
}
