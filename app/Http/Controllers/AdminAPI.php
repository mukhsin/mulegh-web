<?php

namespace App\Http\Controllers;

use App\Cabang;
use App\Http\Requests;
use App\User;
use App\Utils;
use Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AdminAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = array();
        foreach (User::where('role', 'admin')->get() as $admin) {
            array_push($admins, $admin);
        }

        return response()->json([
            'error' => false,
            'admins' => Utils::paginate($admins, 10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'error' => false,
            'cabangs' => Cabang::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin = User::create([
            'username' => $request->username,
            'password' => Hash::make($request->username.'12345'),
            'nama' => $request->nama,
            'role' => 'admin',
            'cabang_id' => $request->cabang_id,
            'api_token' => str_random(60),
        ]);

        $admins = array();
        foreach (User::where('role', 'admin')->get(), as $i => $user) {
            array_push($admins, $user);
        }

        if ($admin) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menambah admin.',
                'admins' => Utils::paginate($admins, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menambah admin.',
                'admins' => Utils::paginate($admins, 10),
            ]);
        }
    }

    /**
     * Display the searched resource.
     *
     * @param  int  $query
     * @return \Illuminate\Http\Response
     */
    public function search($query)
    {
        $array = User::where('username', 'like', $query)
                        ->orWhere('username', 'like', $query.'%')
                        ->orWhere('username', 'like', '%'.$query)
                        ->orWhere('username', 'like', '%'.$query.'%')
                        ->orWhere('nama', 'like', $query)
                        ->orWhere('nama', 'like', $query.'%')
                        ->orWhere('nama', 'like', '%'.$query)
                        ->orWhere('nama', 'like', '%'.$query.'%')
                        ->get();

        $admins = array();
        foreach ($array as $a) {
            array_push($admins, $a);
        }

        return response()->json([
            'error' => false,
            'admins' => Utils::paginate($admins, 10),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'error' => false,
            'admin' => User::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json([
            'error' => false,
            'cabangs' => Cabang::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = User::find($id);
        $admin->cabang_id = $request->cabang_id;
        $admin->save();

        $admins = array();
        foreach (User::where('role', 'admin')->get(), as $i => $user) {
            array_push($admins, $user);
        }

        if ($admin) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil mengubah admin.',
                'admins' => Utils::paginate($admins, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat mengubah admin.',
                'admins' => Utils::paginate($admins, 10),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = User::find($id);
        $admin->delete();

        $admins = array();
        foreach (User::where('role', 'admin')->get(), as $i => $user) {
            array_push($admins, $user);
        }

        if ($admin) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menghapus admin.',
                'admins' => Utils::paginate($admins, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menghapus admin.',
                'admins' => Utils::paginate($admins, 10),
            ]);
        }
    }
}
