<?php

namespace App\Http\Controllers;

use App\Botol;
use App\Cabang;
use App\Http\Requests;
use App\Utils;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BotolAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $botols = array();
        foreach (Botol::orderBy('tipe', 'asc')->get() as $botol) {
            array_push($botols, $botol);
        }

        return response()->json([
            'error' => false,
            'botols' => Utils::paginate($botols, 10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('botol.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $botol = Botol::create([
            'tipe' => $request->tipe,
            'harga' => $request->harga,
            'stock' => 0,
            'cabang_id' => $request->user()->cabang_id,
            'author' => $request->user()->username,
        ]);

        $botols = array();
        foreach (Botol::orderBy('tipe', 'asc')->get(), as $botol) {
            array_push($botols, $botol);
        }

        if ($botol) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menambah botol.',
                'botols' => Utils::paginate($botols, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menambah botol.',
                'botols' => Utils::paginate($botols, 10),
            ]);
        }
    }

    /**
     * Display the searched resource.
     *
     * @param  int  $query
     * @return \Illuminate\Http\Response
     */
    public function search($query)
    {
        $array = Botol::where('tipe', 'like', $query)
                    ->orWhere('tipe', 'like', $query.'%')
                    ->orWhere('tipe', 'like', '%'.$query)
                    ->orWhere('tipe', 'like', '%'.$query.'%')
                    ->orWhere('harga', 'like', $query)
                    ->orWhere('harga', 'like', $query.'%')
                    ->orWhere('harga', 'like', '%'.$query)
                    ->orWhere('harga', 'like', '%'.$query.'%')
                    ->orWhere('stock', 'like', $query)
                    ->orWhere('stock', 'like', $query.'%')
                    ->orWhere('stock', 'like', '%'.$query)
                    ->orWhere('stock', 'like', '%'.$query.'%')
                    ->get();

        $botols = array();
        foreach ($array as $a) {
            array_push($botols, $a);
        }

        return response()->json([
            'error' => false,
            'botols' => Utils::paginate($botols, 10),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'error' => false,
            'botols' => Botol::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return view('botol.edit', ['botol' => Botol::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tipe' => 'required|max:255',
            'harga' => 'required|max:255',
        ]);

        $botol = Botol::find($id);
        $botol->tipe = $request->tipe;
        $botol->harga = $request->harga;
        $botol->author = $request->user()->username;
        $botol->save();

        $botols = array();
        foreach (Botol::orderBy('tipe', 'asc')->get(), as $botol) {
            array_push($botols, $botol);
        }

        if ($botol) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil mengubah botol.',
                'botols' => Utils::paginate($botols, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat mengubah botol.',
                'botols' => Utils::paginate($botols, 10),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $botol = Botol::find($id);
        $botol->delete();

        $botols = array();
        foreach (Botol::orderBy('tipe', 'asc')->get(), as $botol) {
            array_push($botols, $botol);
        }

        if ($botol) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menghapus botol.',
                'botols' => Utils::paginate($botols, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menghapus botol.',
                'botols' => Utils::paginate($botols, 10),
            ]);
        }
    }
}
