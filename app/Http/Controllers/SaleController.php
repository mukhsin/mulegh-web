<?php

namespace App\Http\Controllers;

use App\Botol;
use App\Customer;
use App\Http\Requests;
use App\Sale;
use App\Utils;
use App\Variant;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = array();
        foreach (Sale::orderBy('updated_at', 'desc')->get() as $sale) {
            array_push($sales, $sale);
        }

        return view('sale.index', [
            'sales' => Utils::paginate($sales, 10),
            'query' => '',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sale.create', [
            'customers' => Customer::all(),
            'botols' => Botol::all(),
            'variants' => Variant::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required|max:255',
            'variant_id' => 'required|max:255',
            'botol_id' => 'required|max:255',
            'mililiter' => 'required|max:255',
            'totalmililiter' => 'required|max:255',
            'variant_harga' => 'required|max:255',
            'botol_harga' => 'required|max:255',
            'harga' => 'required|max:255',
            'diskon' => 'required|max:255',
            'total' => 'required|max:255',
        ]);

        $sale = Sale::create([
            'customer_id' => $request->customer_id,
            'variant_id' => $request->variant_id,
            'botol_id' => $request->botol_id,
            'mililiter' => $request->mililiter,
            'totalmililiter' => $request->totalmililiter,
            'variant_harga' => $request->variant_harga,
            'botol_harga' => $request->botol_harga,
            'harga' => $request->harga,
            'diskon' => $request->diskon,
            'total' => $request->total,
            'status' => 1,
            'author' => Auth::user()->username,
        ]);

        if ($sale) {
            return redirect(url('/sale'))->with('sukses', 'Berhasil menambah sale.');
        } else {
            return redirect(url('/sale'))->with('gagal', 'Terjadi kesalahan saat menambah sale.');
        }
    }

    /**
     * Display the searched resource.
     *
     * @param  int  $query
     * @return \Illuminate\Http\Response
     */
    public function search($query)
    {
        $sql = "SELECT sales.id FROM sales
            	JOIN customers ON customers.id = sales.customer_id
                JOIN variants ON variants.id = sales.variant_id
                JOIN botols ON botols.id = sales.botol_id
                WHERE customers.nomor LIKE '$query'
                OR customers.nomor LIKE '%$query'
                OR customers.nomor LIKE '$query%'
                OR customers.nama LIKE '$query'
                OR customers.nama LIKE '%$query'
                OR customers.nama LIKE '$query%'
                OR variants.kode LIKE '$query'
                OR variants.kode LIKE '%$query'
                OR variants.kode LIKE '$query%'
                OR variants.nama LIKE '$query'
                OR variants.nama LIKE '%$query'
                OR variants.nama LIKE '$query%'
                OR botols.tipe LIKE '$query'
                OR botols.tipe LIKE '%$query'
                OR botols.tipe LIKE '$query%'
                OR sales.mililiter LIKE '$query'
                OR sales.mililiter LIKE '%$query'
                OR sales.mililiter LIKE '$query%'
                OR sales.totalmililiter LIKE '$query'
                OR sales.totalmililiter LIKE '%$query'
                OR sales.totalmililiter LIKE '$query%'
                OR sales.harga LIKE '$query'
                OR sales.harga LIKE '%$query'
                OR sales.harga LIKE '$query%'
                OR sales.diskon LIKE '$query'
                OR sales.diskon LIKE '%$query'
                OR sales.diskon LIKE '$query%'
                OR sales.updated_at LIKE '$query'
                OR sales.updated_at LIKE '%$query'
                OR sales.updated_at LIKE '$query%'";
        $results = \DB::select($sql);

        $sales = array();
        foreach ($results as $result) {
            $sale = Sale::find($result->id);
            array_push($sales, $sale);
        }

        $sales = Utils::paginate($sales, 10);

        return view('sale.index', [
            'sales' => $sales,
            'query' => $query,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
