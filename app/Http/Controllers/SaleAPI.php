<?php

namespace App\Http\Controllers;

use App\Botol;
use App\Customer;
use App\Http\Requests;
use App\Sale;
use App\Utils;
use App\Variant;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class SaleAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = array();
        foreach (Sale::orderBy('updated_at', 'desc')->get() as $sale) {
            array_push($sales, $sale);
        }

        return response()->json([
            'error' => false,
            'sales' => Utils::paginate($sales, 10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'error' => false,
            'customers' => Customer::all(),
            'botols' => Botol::all(),
            'variants' => Variant::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sale = Sale::create([
            'customer_id' => $request->customer_id,
            'variant_id' => $request->variant_id,
            'botol_id' => $request->botol_id,
            'mililiter' => $request->mililiter,
            'totalmililiter' => $request->totalmililiter,
            'variant_harga' => $request->variant_harga,
            'botol_harga' => $request->botol_harga,
            'harga' => $request->harga,
            'diskon' => $request->diskon,
            'total' => $request->total,
            'status' => 1,
            'author' => $request->user()->username,
        ]);

        $sales = array();
        foreach (Sale::orderBy('updated_at', 'desc')->get() as $sale) {
            array_push($sales, $sale);
        }

        if ($sale) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menambah sale.',
                'sales' => Utils::paginate($sales, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menambah sale.',
                'sales' => Utils::paginate($sales, 10),
            ]);
        }
    }

    /**
     * Display the searched resource.
     *
     * @param  int  $query
     * @return \Illuminate\Http\Response
     */
    public function search($query)
    {
        $sql = "SELECT sales.id FROM sales
            	JOIN customers ON customers.id = sales.customer_id
                JOIN variants ON variants.id = sales.variant_id
                JOIN botols ON botols.id = sales.botol_id
                WHERE customers.nomor LIKE '$query'
                OR customers.nomor LIKE '%$query'
                OR customers.nomor LIKE '$query%'
                OR customers.nama LIKE '$query'
                OR customers.nama LIKE '%$query'
                OR customers.nama LIKE '$query%'
                OR variants.kode LIKE '$query'
                OR variants.kode LIKE '%$query'
                OR variants.kode LIKE '$query%'
                OR variants.nama LIKE '$query'
                OR variants.nama LIKE '%$query'
                OR variants.nama LIKE '$query%'
                OR botols.tipe LIKE '$query'
                OR botols.tipe LIKE '%$query'
                OR botols.tipe LIKE '$query%'
                OR sales.mililiter LIKE '$query'
                OR sales.mililiter LIKE '%$query'
                OR sales.mililiter LIKE '$query%'
                OR sales.totalmililiter LIKE '$query'
                OR sales.totalmililiter LIKE '%$query'
                OR sales.totalmililiter LIKE '$query%'
                OR sales.harga LIKE '$query'
                OR sales.harga LIKE '%$query'
                OR sales.harga LIKE '$query%'
                OR sales.diskon LIKE '$query'
                OR sales.diskon LIKE '%$query'
                OR sales.diskon LIKE '$query%'
                OR sales.updated_at LIKE '$query'
                OR sales.updated_at LIKE '%$query'
                OR sales.updated_at LIKE '$query%'";
        $results = \DB::select($sql);

        $sales = array();
        foreach ($results as $result) {
            $sale = Sale::find($result->id);
            array_push($sales, $sale);
        }

        $sales = Utils::paginate($sales, 10);

        return response()->json([
            'error' => false,
            'sales' => $sales,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
