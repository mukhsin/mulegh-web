<?php

namespace App\Http\Controllers;

use App\Cabang;
use App\Customer;
use App\Http\Requests;
use App\Utils;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CustomerAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = array();
        foreach (Customer::orderBy('nomor', 'asc')->get() as $customer) {
            array_push($customers, $customer);
        }

        return response()->json([
            'error' => false,
            'customers' => Utils::paginate($customers, 10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nomor = Customer::orderBy('id', 'desc')->first()->nomor;
        $nomor_baru = Utils::intTo5(intval(substr($nomor, -5)) + 1);

        return response()->json([
            'error' => false,
            'nomor_baru' => Cabang::find(Auth::user()->cabang_id)->kode.$nomor_baru,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = Customer::create([
            'nomor' => $request->nomor,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'telepon' => $request->telepon,
            'cabang_id' => $request->user()->cabang_id,
            'author' => $request->user()->username,
        ]);

        $customers = array();
        foreach (Customer::all() as $customer) {
            array_push($customers, $customer);
        }

        if ($customer) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menambah customer.',
                'customers' => Utils::paginate($customers, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menambah customer.',
                'customers' => Utils::paginate($customers, 10),
            ]);
        }
    }

    /**
     * Display the searched resource.
     *
     * @param  int  $query
     * @return \Illuminate\Http\Response
     */
    public function search($query)
    {
        $array = Customer::where('nomor', 'like', $query)
                            ->orWhere('nomor', 'like', $query.'%')
                            ->orWhere('nomor', 'like', '%'.$query)
                            ->orWhere('nomor', 'like', '%'.$query.'%')
                            ->orWhere('nama', 'like', $query)
                            ->orWhere('nama', 'like', $query.'%')
                            ->orWhere('nama', 'like', '%'.$query)
                            ->orWhere('nama', 'like', '%'.$query.'%')
                            ->orWhere('alamat', 'like', $query)
                            ->orWhere('alamat', 'like', $query.'%')
                            ->orWhere('alamat', 'like', '%'.$query)
                            ->orWhere('alamat', 'like', '%'.$query.'%')
                            ->orWhere('telepon', 'like', $query)
                            ->orWhere('telepon', 'like', $query.'%')
                            ->orWhere('telepon', 'like', '%'.$query)
                            ->orWhere('telepon', 'like', '%'.$query.'%')
                            ->get();

        $customers = array();
        foreach ($array as $a) {
            array_push($customers, $a);
        }

        return response()->json([
            'error' => false,
            'customers' => Utils::paginate($customers, 10),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'error' => false,
            'customer' => Customer::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return view('customer.edit', ['customer' => Customer::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->nama = $request->nama;
        $customer->alamat = $request->alamat;
        $customer->telepon = $request->telepon;
        $customer->cabang_id = $request->user()->cabang_id;
        $customer->author = $request->user()->username;
        $customer->save();

        $customers = array();
        foreach (Customer::all() as $user) {
            array_push($customers, $user);
        }

        if ($customer) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil mengubah customer.',
                'customers' => Utils::paginate($customers 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat mengubah customer.',
                'customers' => Utils::paginate($customers 10),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();

        $customers = array();
        foreach (Customer::all() as $user) {
            array_push($customers, $user);
        }

        if ($customer) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menghapus customer.',
                'customers' => Utils::paginate($customers 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menghapus customer.',
                'customers' => Utils::paginate($customers 10),
            ]);
        }
    }
}
