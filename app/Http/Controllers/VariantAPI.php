<?php

namespace App\Http\Controllers;

use App\Cabang;
use App\Http\Requests;
use App\Utils;
use App\Variant;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class VariantAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $variants = array();
        foreach (Variant::orderBy('kode', 'asc')->get() as $variant) {
            array_push($variants, $variant);
        }

        return response()->json([
            'error' => false,
            'variants' => Utils::paginate($variants, 10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('variant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $variant = Variant::create([
            'kode' => $request->kode,
            'nama' => $request->nama,
            'harga' => $request->harga,
            'stock' => 0,
            'cabang_id' => $request->user()->cabang_id,
            'author' => $request->user()->username,
        ]);

        $variants = array();
        foreach (Variant::orderBy('kode', 'asc')->get() as $variant) {
            array_push($variants, $variant);
        }

        if ($variant) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menambah variant.',
                'variants' => Utils::paginate($variants, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menambah variant.',
                'variants' => Utils::paginate($variants, 10),
            ]);
        }
    }

    /**
     * Display the searched resource.
     *
     * @param  int  $query
     * @return \Illuminate\Http\Response
     */
    public function search($query)
    {
        $array = Variant::where('kode', 'like', $query)
                        ->orWhere('kode', 'like', $query.'%')
                        ->orWhere('kode', 'like', '%'.$query)
                        ->orWhere('kode', 'like', '%'.$query.'%')
                        ->orWhere('nama', 'like', $query)
                        ->orWhere('nama', 'like', $query.'%')
                        ->orWhere('nama', 'like', '%'.$query)
                        ->orWhere('nama', 'like', '%'.$query.'%')
                        ->orWhere('harga', 'like', $query)
                        ->orWhere('harga', 'like', $query.'%')
                        ->orWhere('harga', 'like', '%'.$query)
                        ->orWhere('harga', 'like', '%'.$query.'%')
                        ->orWhere('stock', 'like', $query)
                        ->orWhere('stock', 'like', $query.'%')
                        ->orWhere('stock', 'like', '%'.$query)
                        ->orWhere('stock', 'like', '%'.$query.'%')
                        ->get();

        $variants = array();
        foreach ($array as $a) {
            array_push($variants, $a);
        }

        return response()->json([
            'error' => false,
            'variants' => Utils::paginate($variants, 10),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'error' => false,
            'variant' => Variant::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return view('variant.edit', ['variant' => Variant::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $variant = Variant::find($id);
        $variant->kode = $request->kode;
        $variant->nama = $request->nama;
        $variant->harga = $request->harga;
        $variant->author = $request->user()->username;
        $variant->save();

        $variants = array();
        foreach (Variant::orderBy('kode', 'asc')->get() as $variant) {
            array_push($variants, $variant);
        }

        if ($variant) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil mengubah variant.',
                'variants' => Utils::paginate($variants, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat mengubah variant.',
                'variants' => Utils::paginate($variants, 10),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $variant = Variant::find($id);
        $variant->delete();

        $variants = array();
        foreach (Variant::orderBy('kode', 'asc')->get() as $variant) {
            array_push($variants, $variant);
        }

        if ($variant) {
            return response()->json([
                'error' => false,
                'message' => 'Berhasil menghapus variant.',
                'variants' => Utils::paginate($variants, 10),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Terjadi kesalahan saat menghapus variant.',
                'variants' => Utils::paginate($variants, 10),
            ]);
        }
    }
}
