<?php

namespace App\Http\Controllers;

use App\Cabang;
use App\Http\Requests;
use App\Utils;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CabangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cabangs = array();
        foreach (Cabang::all() as $cabang) {
            array_push($cabangs, $cabang);
        }

        return view('cabang.index', [
            'cabangs' => Utils::paginate($cabangs, 10),
            'query' => '',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cabang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required|max:255',
            'nama' => 'required|max:255',
            'alamat' => 'required|max:255',
            'telepon' => 'required|max:255',
        ]);

        $cabang = Cabang::create([
            'kode' => $request->kode,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'telepon' => $request->telepon,
        ]);

        if ($cabang) {
            return redirect(url('/cabang'))->with('sukses', 'Berhasil menambah cabang.');
        } else {
            return redirect(url('/cabang'))->with('gagal', 'Terjadi kesalahan saat menambah cabang.');
        }
    }

    /**
     * Display the searched resource.
     *
     * @param  int  $query
     * @return \Illuminate\Http\Response
     */
    public function search($query)
    {
        $array = Cabang::where('kode', 'like', $query)
                        ->orWhere('kode', 'like', $query.'%')
                        ->orWhere('kode', 'like', '%'.$query)
                        ->orWhere('kode', 'like', '%'.$query.'%')
                        ->orWhere('nama', 'like', $query)
                        ->orWhere('nama', 'like', $query.'%')
                        ->orWhere('nama', 'like', '%'.$query)
                        ->orWhere('nama', 'like', '%'.$query.'%')
                        ->orWhere('alamat', 'like', $query)
                        ->orWhere('alamat', 'like', $query.'%')
                        ->orWhere('alamat', 'like', '%'.$query)
                        ->orWhere('alamat', 'like', '%'.$query.'%')
                        ->orWhere('telepon', 'like', $query)
                        ->orWhere('telepon', 'like', $query.'%')
                        ->orWhere('telepon', 'like', '%'.$query)
                        ->orWhere('telepon', 'like', '%'.$query.'%')
                        ->get();

        $cabangs = array();
        foreach ($array as $a) {
            array_push($cabangs, $a);
        }

        return view('cabang.index', [
            'cabangs' => Utils::paginate($cabangs, 10),
            'query' => $query,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('cabang.show', ['cabang' => Cabang::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('cabang.edit', ['cabang' => Cabang::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'kode' => 'required|max:255',
            'nama' => 'required|max:255',
            'alamat' => 'required|max:255',
            'telepon' => 'required|max:255',
        ]);

        $cabang = Cabang::find($id);
        $cabang->kode = $request->kode;
        $cabang->nama = $request->nama;
        $cabang->alamat = $request->alamat;
        $cabang->telepon = $request->telepon;
        $cabang->save();

        if ($cabang) {
            return redirect(url('/cabang'))->with('sukses', 'Berhasil mengubah cabang.');
        } else {
            return redirect(url('/cabang'))->with('gagal', 'Terjadi kesalahan saat mengubah cabang.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cabang = Cabang::find($id);
        $cabang->delete();

        if ($cabang) {
            return redirect(url('/cabang'))->with('sukses', 'Berhasil menghapus cabang.');
        } else {
            return redirect(url('/cabang'))->with('gagal', 'Terjadi kesalahan saat menghapus cabang.');
        }
    }
}
