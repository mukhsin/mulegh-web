<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode', 'nama', 'alamat', 'telepon'
    ];

    public function botols()
    {
        return $this->hasMany('App\Botol');
    }

    public function customers()
    {
        return $this->hasMany('App\Customer');
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function variants()
    {
        return $this->hasMany('App\Variant');
    }
}
