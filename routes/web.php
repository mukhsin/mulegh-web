<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*
|--------------------------------------------------------------------------
| Route untuk home
|--------------------------------------------------------------------------
*/
Route::get('/', 'HomeController@get')->name('root');
Route::get('/home', 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| Route untuk login dan logout
|--------------------------------------------------------------------------
*/
Route::get('/login', 'AuthController@getLogin')->name('login.get');
Route::post('/login', 'AuthController@postLogin')->name('login.post');
Route::get('/logout', 'AuthController@getLogout')->name('logout.get');
Route::post('/logout', 'AuthController@postLogout')->name('logout.post');

Route::group(['middleware' => 'auth'], function() {

    /*
    |--------------------------------------------------------------------------
    | Route untuk role bos
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role:bos'], function() {

        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel cabangs
        |--------------------------------------------------------------------------
        */
        Route::resource('cabang', 'CabangController');
        Route::get('cabang/search/{search}', 'CabangController@search')->name('cabang.search');

        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel users
        |--------------------------------------------------------------------------
        */
        Route::resource('admin', 'AdminController');
        Route::get('admin/search/{search}', 'AdminController@search')->name('admin.search');
    });

    /*
    |--------------------------------------------------------------------------
    | Route untuk role admin
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role:admin'], function() {
        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel sales
        |--------------------------------------------------------------------------
        */
        Route::post('/sale', 'SaleController@store')->name('sale.store');
        Route::get('/sale/create', 'SaleController@create')->name('sale.create');

        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel customers
        |--------------------------------------------------------------------------
        */
        Route::resource('/customer', 'CustomerController', ['except' => ['index', 'show']]);

        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel variants
        |--------------------------------------------------------------------------
        */
        Route::resource('/variant', 'VariantController', ['except' => ['index', 'show']]);
        Route::post('/variant/{variant}/stock', 'VariantStockController@store')->name('variant.stock.store');
        Route::get('/variant/{variant}/stock/create', 'VariantStockController@create')->name('variant.stock.create');

        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel botols
        |--------------------------------------------------------------------------
        */
        Route::resource('/botol', 'BotolController', ['except' => ['index', 'show']]);
        Route::post('/botol/{botol}/stock', 'BotolStockController@store')->name('botol.stock.store');
        Route::get('/botol/{botol}/stock/create', 'BotolStockController@create')->name('botol.stock.create');
    });

    /*
    |--------------------------------------------------------------------------
    | Route untuk tabel sales
    |--------------------------------------------------------------------------
    */
    Route::get('/sale', 'SaleController@index')->name('sale.index');
    Route::get('/sale/search/{search}', 'SaleController@search')->name('sale.search');

    /*
    |--------------------------------------------------------------------------
    | Route untuk tabel customers
    |--------------------------------------------------------------------------
    */
    Route::resource('/customer', 'CustomerController', ['only' => ['index', 'show']]);
    Route::get('/customer/search/{search}', 'CustomerController@search')->name('customer.search');

    /*
    |--------------------------------------------------------------------------
    | Route untuk tabel variants
    |--------------------------------------------------------------------------
    */
    Route::resource('/variant', 'VariantController', ['only' => ['index', 'show']]);
    Route::get('/variant/search/{search}', 'VariantController@search')->name('variant.search');
    Route::get('/variant/{variant}/stock', 'VariantStockController@index')->name('variant.stock.index');

    /*
    |--------------------------------------------------------------------------
    | Route untuk tabel botols
    |--------------------------------------------------------------------------
    */
    Route::resource('/botol', 'BotolController', ['only' => ['index', 'show']]);
    Route::get('/botol/search/{search}', 'BotolController@search')->name('botol.search');
    Route::get('/botol/{botol}/stock', 'BotolStockController@index')->name('botol.stock.index');

    /*
    |--------------------------------------------------------------------------
    | Route untuk tabel users
    |--------------------------------------------------------------------------
    */
    Route::get('/profil', 'UserController@getProfil')->name('profil.edit');
    Route::put('/profil', 'UserController@putProfil')->name('profil.update');
    Route::get('/sandi', 'UserController@getSandi')->name('sandi.edit');
    Route::put('/sandi', 'UserController@putSandi')->name('sandi.update');
});

// Route::get('/migrate/botol', 'MigrateController@botol');
// Route::get('/migrate/variant', 'MigrateController@variant');
// Route::get('/migrate/customer', 'MigrateController@customer');
// Route::get('/migrate/stock', 'MigrateController@stock');
// Route::get('/migrate/sale', 'MigrateController@sale');
// Route::get('/migrate/stock/optimize', 'MigrateController@optimizeStock');
// Route::get('/migrate/sale/optimize', 'MigrateController@optimizeSale');
// Route::get('/migrate/customer/optimize', 'MigrateController@optimizeCustomer');
// Route::get('/migrate/user/optimize', 'MigrateController@optimizeUser');
// Route::get('/migrate/variant/optimize', 'MigrateController@optimizeVariant');
