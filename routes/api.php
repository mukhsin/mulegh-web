<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');

/*
|--------------------------------------------------------------------------
| Route untuk login dan logout
|--------------------------------------------------------------------------
*/
Route::post('/login', 'AuthAPI@login')->name('api.login.post');

Route::group(['middleware' => 'auth:api'], function() {

    /*
    |--------------------------------------------------------------------------
    | Route untuk role bos
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role.api:bos'], function() {

        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel cabangs
        |--------------------------------------------------------------------------
        */
        Route::resource('cabang', 'CabangAPI', [
            'except' => ['create', 'edit'],
            'as' => 'api',
        ]);
        Route::get('cabang/search/{search}', 'CabangAPI@search')->name('api.cabang.search');

        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel users
        |--------------------------------------------------------------------------
        */
        Route::resource('admin', 'AdminAPI', ['as' => 'api',]);
        Route::get('admin/search/{search}', 'AdminAPI@search')->name('api.admin.search');
    });

    /*
    |--------------------------------------------------------------------------
    | Route untuk role admin
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role.api:admin'], function() {
        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel sales
        |--------------------------------------------------------------------------
        */
        Route::post('/sale', 'SaleAPI@store')->name('api.sale.store');
        Route::get('/sale/create', 'SaleAPI@create')->name('api.sale.create');

        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel customers
        |--------------------------------------------------------------------------
        */
        Route::resource('/customer', 'CustomerAPI', [
            'except' => ['index', 'show', 'edit'],
            'as' => 'api',
        ]);

        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel variants
        |--------------------------------------------------------------------------
        */
        Route::resource('/variant', 'VariantAPI', [
            'except' => ['index', 'create', 'show', 'edit'],
            'as' => 'api',
        ]);
        Route::post('/variant/{variant}/stock', 'VariantStockAPI@store')->name('api.variant.stock.store');
        Route::get('/variant/{variant}/stock/create', 'VariantStockAPI@create')->name('api.variant.stock.create');

        /*
        |--------------------------------------------------------------------------
        | Route untuk tabel botols
        |--------------------------------------------------------------------------
        */
        Route::resource('/botol', 'BotolAPI', [
            'except' => ['index', 'create', 'show', 'edit'],
            'as' => 'api',
        ]);
        Route::post('/botol/{botol}/stock', 'BotolStockAPI@store')->name('api.botol.stock.store');
        Route::get('/botol/{botol}/stock/create', 'BotolStockAPI@create')->name('api.botol.stock.create');
    });

    /*
    |--------------------------------------------------------------------------
    | Route untuk tabel sales
    |--------------------------------------------------------------------------
    */
    Route::get('/sale', 'SaleAPI@index')->name('api.sale.index');
    Route::get('/sale/search/{search}', 'SaleAPI@search')->name('api.sale.search');

    /*
    |--------------------------------------------------------------------------
    | Route untuk tabel customers
    |--------------------------------------------------------------------------
    */
    Route::resource('/customer', 'CustomerAPI', [
        'only' => ['index', 'show'],
        'as' => 'api',
    ]);
    Route::get('/customer/search/{search}', 'CustomerAPI@search')->name('api.customer.search');

    /*
    |--------------------------------------------------------------------------
    | Route untuk tabel variants
    |--------------------------------------------------------------------------
    */
    Route::resource('/variant', 'VariantAPI', [
        'only' => ['index', 'show'],
        'as' => 'api',
    ]);
    Route::get('/variant/search/{search}', 'VariantAPI@search')->name('api.variant.search');
    Route::get('/variant/{variant}/stock', 'VariantStockAPI@index')->name('api.variant.stock.index');

    /*
    |--------------------------------------------------------------------------
    | Route untuk tabel botols
    |--------------------------------------------------------------------------
    */
    Route::resource('/botol', 'BotolAPI', [
        'only' => ['index', 'show'],
        'as' => 'api',
    ]);
    Route::get('/botol/search/{search}', 'BotolAPI@search')->name('api.botol.search');
    Route::get('/botol/{botol}/stock', 'BotolStockAPI@index')->name('api.botol.stock.index');

    /*
    |--------------------------------------------------------------------------
    | Route untuk tabel users
    |--------------------------------------------------------------------------
    */
    Route::get('/profil', 'UserAPI@getProfil')->name('api.profil.edit');
    Route::put('/profil', 'UserAPI@putProfil')->name('api.profil.update');
    Route::get('/sandi', 'UserAPI@getSandi')->name('api.sandi.edit');
    Route::put('/sandi', 'UserAPI@putSandi')->name('api.sandi.update');
});
