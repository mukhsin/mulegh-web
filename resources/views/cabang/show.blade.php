@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Mulegh</a></li>
            <li><a href="{{ url('/cabang') }}">Cabang</a></li>
            <li class="active">Detail</li>
        </ol>

        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="table-title">
                    Detail Cabang
                </span>
            </div>

            <div class="panel-body">
                <table class="table table-bordered table-hover table-striped">
                    <tbody>
                        <tr>
                            <td>Kode Cabang</td>
                            <td>{{ $cabang->kode }}</td>
                        </tr>
                        <tr>
                            <td>Nama Cabang</td>
                            <td>{{ $cabang->nama }}</td>
                        </tr>
                        <tr>
                            <td>Alamat Cabang</td>
                            <td>{{ $cabang->alamat }}</td>
                        </tr>
                        <tr>
                            <td>Telepon Cabang</td>
                            <td>{{ $cabang->telepon }}</td>
                        </tr>
                        <tr>
                            <td>Created At</td>
                            <td>{{ App\Utils::timestampToString($cabang->created_at) }}</td>
                        </tr>
                        <tr>
                            <td>Updated At</td>
                            <td>{{ App\Utils::timestampToString($cabang->updated_at) }}</td>
                        </tr>
                    </tbody>
                </table>

                <a href="{{ URL::previous() }}">
                    <button type="button" name="button" class="btn btn-default">
                        <i class="fa fa-angle-double-left"></i>
                        Kembali
                    </button>
                </a>
            </div>
        </div>
    </div>
@endsection
