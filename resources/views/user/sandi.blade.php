@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="table-title">
                    Ubah Sandi
                </span>
            </div>

            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/sandi') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="put">

                    <div class="form-group{{ $errors->has('sandi_lama') ? ' has-error' : '' }}">
                        <label for="sandi_lama" class="col-md-4 control-label">Sandi Lama</label>

                        <div class="col-md-6">
                            <input id="sandi_lama" type="password" class="form-control" name="sandi_lama" required="" autofocus="">

                            @if ($errors->has('sandi_lama'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('sandi_lama') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('sandi_baru') ? ' has-error' : '' }}">
                        <label for="sandi_baru" class="col-md-4 control-label">Sandi Baru</label>

                        <div class="col-md-6">
                            <input id="sandi_baru" type="password" class="form-control" name="sandi_baru" required="">

                            @if ($errors->has('sandi_baru'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('sandi_baru') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('sandi_baru_confirmation') ? ' has-error' : '' }}">
                        <label for="sandi_baru_confirmation" class="col-md-4 control-label">Konfirmasi Sandi Baru</label>

                        <div class="col-md-6">
                            <input id="sandi_baru_confirmation" type="password" class="form-control" name="sandi_baru_confirmation" required="">

                            @if ($errors->has('sandi_baru_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('sandi_baru_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
