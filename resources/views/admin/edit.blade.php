@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Mulegh</a></li>
            <li><a href="{{ url('/admin') }}">Admin</a></li>
            <li class="active">Ubah</li>
        </ol>
    </div>

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="table-title">
                    Ubah Admin
                </span>
            </div>

            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/'.$admin->id) }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="url" value="{{ URL::previous() }}">

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-4 control-label">Username</label>

                        <div class="col-md-8">
                            <input type="text" class="form-control" name="username" value="{{ $admin->username }}" required="" disabled="">

                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <label for="nama" class="col-md-4 control-label">Nama Lengkap</label>

                        <div class="col-md-8">
                            <input type="text" class="form-control" name="nama" value="{{ $admin->nama }}" required="" disabled="">

                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nama') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('cabang_id') ? ' has-error' : '' }}">
                        <label for="cabang_id" class="col-md-4 control-label">Cabang</label>

                        <div class="col-md-8">
                            <select class="form-control" name="cabang_id" required="">
                                <option value="">Cabang</option>
                                @foreach ($cabangs as $i => $cabang)
                                    @if ($cabang->id == $admin->cabang_id)
                                        <option value="{{ $cabang->id }}" selected="">{{ $cabang->nama }}</option>
                                    @else
                                        <option value="{{ $cabang->id }}">{{ $cabang->nama }}</option>
                                    @endif

                                @endforeach
                            </select>

                            @if ($errors->has('cabang_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('cabang_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    {{-- <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
                        <label for="author" class="col-md-4 control-label">Author</label>

                        <div class="col-md-8">
                            <input type="text" class="form-control" name="author" value="{{ $author }}" readonly="" required="">

                            @if ($errors->has('author'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('author') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> --}}

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <a href="{{ URL::previous() }}">
                                <button type="button" class="btn btn-default">
                                    <i class="fa fa-btn fa-angle-double-left"></i>
                                    Kembali
                                </button>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-save"></i>
                                Simpan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
